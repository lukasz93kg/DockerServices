#!/bin/sh

set -e

# Start the ssh server
echo "Runing ssh server"
/etc/init.d/ssh start

echo "Checking ssh server"
/etc/init.d/ssh status || (>&2 echo "SSH services is not running" &&  exit 1)

# Execute the CMD
exec "$@"
