#!/bin/sh

set -e

# Start the http server
echo "Runing http server"
/etc/init.d/apache2 start

echo "Checking http server"
/etc/init.d/apache2 status || (>&2 echo "HTTP services is not running" &&  exit 1)

# Execute the CMD
exec "$@"
