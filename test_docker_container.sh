#!/bin/bash

docker-compose up -d

#get docker ip
docker_ip="$(ifconfig docker | grep "inet addr:" | cut -d: -f2 | awk '{print $1}')"
url="http://127.0.0.1:85"
ssh_port=25
#pyscrcipt with parameter
python3 test_services.py "$docker_ip" "$url" --ssh_port "$ssh_port"
