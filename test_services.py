import argparse
import paramiko
import requests
import sys


class TestServices():

    def __init__(self, args):
        self.hostname = args[0]
        self.port = args[1]
        self.user_name = args[2]
        self.key_filename = args[3]
        self.timeout = args[4]
        self.web_site = args[5]

    def try_ssh_connection(self):
        client = paramiko.SSHClient()
        client.load_system_host_keys()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.connect(hostname=self.hostname, port=self.port, username=self.user_name,
                       key_filename=self.key_filename, timeout=self.timeout)
        client.close()
        print("SSH connection was successful")

    def try_http_connection(self):
        request = requests.get(self.web_site)
        if not request.ok and request.status_code != 200:
            sys.exit(2)

        print("HTTP connection was successful")

    def get_http_content(self):
        request = requests.get(self.web_site)
        if not request.ok and not request.text:
            sys.exit(4)

        print("Getting HTTP content was successful")
        return request.text


def arg_parse():
    ssh_port = 22
    ssh_user = "root"
    ssh_key = "sshd/key/ssh_key"
    timeout = 3

    parser = argparse.ArgumentParser()
    parser.add_argument("hostname", help="Hostname which we want to connect")
    parser.add_argument("website", help="URL to website for test. E.g 'http://localhost:85'")
    parser.add_argument("--ssh_user", help="User on ssh machine. Default root")
    parser.add_argument("--ssh_port", type=int, help="Port to ssh connection. Default 22")
    parser.add_argument("--ssh_key", help="Path to ssh private key. Default sshd/key/ssh_key")
    parser.add_argument("--timeout", type=int, help="Timeout to check ssh connection. Default 3")
    args = parser.parse_args()
    if args.ssh_port:
        ssh_port = args.ssh_port

    if args.ssh_user:
        ssh_user = args.ssh_user

    if args.timeout:
        timeout = args.timeout

    if args.ssh_key:
        ssh_key = args.ssh_key

    web_site = args.website
    hostname = args.hostname

    return hostname, ssh_port, ssh_user, ssh_key, timeout, web_site


def main():

    test = TestServices(arg_parse())
    test.try_ssh_connection()
    test.try_http_connection()
    test.get_http_content()


if __name__ == "__main__":
    main()
